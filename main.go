package main

import (
	"derivative"
	"fmt"
	"integration"
	"log"
	"math"
	"solving"
	"strings"
)

/* function to create polynomial */
func createPolynomial(coeff []float64) func(x float64) float64 {

	/* make and return polynomial function */
	return func(x float64) float64 {
		i := len(coeff) - 1
		res := 0.0
		for i >= 0 {
			res += coeff[i] * math.Pow(x, float64(i))
			i--
		}
		return res
	}

}

/* function to find derivative for polynomial */
func launchDerivative(coeff []float64, point, prec float64) float64 {

	/* variable declarations */
	var (
		function func(x float64) float64 = createPolynomial(coeff)
		der      *derivative.Derivative
		err      error
	)

	/* create instance of structure for derivative */
	der, err = derivative.MakeDerivative(function, point, prec)
	if err != nil {
		log.Fatal(err)
	}

	/* call function to get answer */
	return der.TakeDerivative()

}

/* function to find integral for polynomial */
func launchIntegration(coeff []float64, begin, end, prec float64) float64 {

	/* variable declarations */
	var (
		err      error
		integ    *integration.Integrator
		function func(x float64) float64 = createPolynomial(coeff)
	)

	/* create instance of structure for integration */
	integ, err = integration.MakeIntegrator(function, begin, end, prec)
	if err != nil {
		log.Fatal(err)
	}

	/* call function to get answer */
	return integ.Integrate()

}

/* function to find roots for polynomial */
func launchSolving(coeff []float64, begin, end, step, prec float64) []float64 {

	/* variable declarations */
	var (
		err      error
		solver   *solving.Solver
		function func(x float64) float64 = createPolynomial(coeff)
	)

	/* create instance of structure for solving */
	solver, err = solving.MakeSolver(function, begin, end, step, prec)
	if err != nil {
		log.Fatal(err)
	}

	/* call function to get answer */
	return solver.Solve()

}

func main() {

	/* variable declarations */
	var (
		coeff             []float64 //coefficients of polynomial
		roots             []float64 //roots of polynomial in some interval
		power             int       //power polynomial
		begin, end        float64   //beginning and end of polynomial for exploration
		point, prec, step float64   //point, precision and step of research for polynomial
		err               error     //instance error type for error analysis
		typeLaunch        string    //type of polynomial research
		result            float64   //result of launch of integrator or finding derivative
	)

	/* read type of launch */
	fmt.Println("Type of launch:")
	_, err = fmt.Scanln(&typeLaunch)
	if err != nil {
		log.Fatal(err)
	}

	/* read power polynomial */
	fmt.Println("Power polynomial:")
	_, err = fmt.Scanln(&power)
	if err != nil {
		log.Fatal(err)
	}

	/* read polynomial coefficients */
	fmt.Println("Polynomial coefficients:")
	coeff = make([]float64, power+1)
	i := power
	for i >= 0 {
		_, err = fmt.Scanf("%f", &coeff[i])
		i--
	}

	/* if type of launch is derivative search */
	if strings.Compare(typeLaunch, "derivative") == 0 {

		/* read point and precision for exploration */
		fmt.Println("Point and precision for derivative:")
		_, err = fmt.Scanln(&point, &prec)
		if err != nil {
			log.Fatal(err)
		}

		/* find answer and write it */
		result = launchDerivative(coeff, point, prec)
		fmt.Println("Derivative is:")
		fmt.Println(result)

		/* if type of launch is integration */
	} else if strings.Compare(typeLaunch, "integration") == 0 {

		/* read begin, end of interval and precision for exploration */
		fmt.Println("Begin, end and precision for integration:")
		_, err = fmt.Scanln(&begin, &end, &prec)
		if err != nil {
			log.Fatal(err)
		}

		/* find answer and write it */
		result = launchIntegration(coeff, begin, end, prec)
		fmt.Println("Integral is:")
		fmt.Println(result)

		/* if type of launch is solving */
	} else if strings.Compare(typeLaunch, "solving") == 0 {

		/* read begin, end of interval, precision and step for exploration */
		fmt.Println("Begin, end, step and precision for solving of equation:")
		_, err = fmt.Scanln(&begin, &end, &step, &prec)
		if err != nil {
			log.Fatal(err)
		}

		/* find roots and write them */
		roots = launchSolving(coeff, begin, end, step, prec)
		fmt.Println("Roots of equation:")
		i = 0
		for i <= len(roots)-1 {
			fmt.Printf("%f ", roots[i])
			i++
		}

		/* if type of launch is uncorrect */
	} else {
		log.Fatal(fmt.Errorf("The name of type to launch is uncorrect!"))
	}

}
