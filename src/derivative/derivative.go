package derivative

import (
	"fmt"
	"math"
)

/* structure to find derivative for some point */
type Derivative struct {
	function func(x float64) float64
	point    float64
	prec     float64
}

/* function to make derivative structure */
func MakeDerivative(function func(x float64) float64, point, prec float64) (*Derivative, error) {

	/* verification of incoming data */
	if function == nil {
		return nil, fmt.Errorf("Function must not be nil!")
	} else if prec <= 0.0 || prec >= 0.2 {
		return nil, fmt.Errorf("Precision must be from 0.0 to 0.2!")
	}

	/* creation derivative structure */
	var der *Derivative = new(Derivative)
	/* initialize values of instance */
	der.function = function
	der.point = point
	der.prec = prec

	/* return instance of structure */
	return der, nil

}

/* method to get derivative */
func (der *Derivative) TakeDerivative() float64 {

	/* calculate step */
	var step float64 = math.Sqrt(der.prec)
	/* calculate derivative and return answer */
	return (der.function(der.point+step) - der.function(der.point-step)) / (2 * step)

}
