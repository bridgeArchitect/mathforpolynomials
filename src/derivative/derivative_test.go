package derivative

import (
	"math"
	"testing"
)

/* function to test derivative package */
func TestDerivative(test *testing.T) {

	/* polynomials to test */
	function1 := func(x float64) float64 {
		return x * x * x
	}

	function2 := func(x float64) float64 {
		return 2 * x * x
	}

	/* declarations of variables */
	var (
		der *Derivative /* instance of derivative structure */
		err error       /* variable to get error */
		res float64     /* obtained result */
	)

	/* create instance of derivative structure */
	der, err = MakeDerivative(function1, 1.0, 0.0001)

	/* checking for creation of structure */
	if err != nil {
		test.Errorf(err.Error())
	}

	/* checking for getting of derivative */
	res = der.TakeDerivative()
	if math.Abs(res-3.0) > 0.0001 {
		test.Errorf("Value must be 3.0! But this value is %f", res)
	}

	/* create new instence of derivative structure */
	der, err = MakeDerivative(function2, 1.0, 0.0001)

	/* checking for creation of structure */
	if err != nil {
		test.Errorf(err.Error())
	}

	/* checking for getting of derivative */
	res = der.TakeDerivative()
	if math.Abs(res-4.0) > 0.0001 {
		test.Errorf("Value must be 4.0! But this value is %f", res)
	}

}
