package solving

import (
	"fmt"
	"math"
)

/* small value to add, so there is no multiplicity of intervals and steps */
const smallValue = 0.00000234591634

/* structure to find roots for some interval */
type Solver struct {
	function   func(x float64) float64
	begin, end float64
	prec, step float64
}

/* function to make structure of solving */
func MakeSolver(function func(x float64) float64, begin, end, step, prec float64) (*Solver, error) {

	/* verification of incoming data */
	if function == nil {
		return nil, fmt.Errorf("Function must not be nil!")
	} else if begin >= end {
		return nil, fmt.Errorf("End must be larger than begin!")
	} else if step > math.Abs(begin-end) && step < 0 {
		return nil, fmt.Errorf("Step must be from 0 to begin-end!")
	} else if prec > step {
		return nil, fmt.Errorf("Precision must be smaller than step!")
	}

	/* creation structure of solving */
	var solver *Solver = new(Solver)
	/* initialize values of instance */
	solver.function = function
	solver.begin = begin
	solver.end = end
	solver.step = step + smallValue
	solver.prec = prec

	/* return instance of structure */
	return solver, nil

}

/* function to find root for some interval */
func findRoot(function func(x float64) float64, begin, end, prec float64) float64 {

	/* point is middle of interval */
	var middle float64
	/* find root with some precision */
	for math.Abs(end-begin) > prec {
		/* use ternary search method */
		middle = (end + begin) / 2.0
		if function(middle)*function(end) >= 0 {
			end = middle
		} else {
			begin = middle
		}
	}

	/* return integral */
	return middle

}

/* method to get roots */
func (solver *Solver) Solve() []float64 {

	var (
		curr   float64     //current point
		amount int     = 0 //amount of roots
		index  int         //index of array for roots
	)

	/* calculate amount of roots */
	curr = solver.begin
	for curr < solver.end {
		/* if signs at ends of interval are different, then add one */
		if solver.function(curr)*solver.function(curr+solver.step) < 0.0 {
			amount++
		}
		curr += solver.step
	}

	/* create array of roots */
	var roots []float64 = make([]float64, amount)

	/* find roots for equation */
	curr = solver.begin
	index = 0
	for curr < solver.end {
		/* if signs at ends of interval are different, then we will run root search for interval */
		if solver.function(curr)*solver.function(curr+solver.step) < 0.0 {
			roots[index] = findRoot(solver.function, curr, curr+solver.step, solver.prec)
			index++
		}
		curr += solver.step
	}

	/* return result */
	return roots

}
