package solving

import (
	"math"
	"testing"
)

func TestSolving(test *testing.T) {

	/* declarations of variables */
	var (
		solver     *Solver                 /* instance of structure to solve equation */
		begin, end float64                 /* begin and end of interval*/
		step, prec float64                 /* step and precision to solve equation */
		function   func(x float64) float64 /* function to solve */
		roots      []float64               /* roots of equation */
		err        error                   /* variable to get error */
	)

	/* initialization of parameters */
	begin = 0.0
	end = 10.0
	step = 0.3
	prec = 0.01
	function = func(x float64) float64 {
		return (x - 7.51) * (x - 1.28)
	}

	/* create instance of structure to solve equations */
	solver, err = MakeSolver(function, begin, end, step, prec)
	/* checking for creation of structure  */
	if err != nil {
		test.Errorf("Running of module was with mistake!")
	}

	/* checking for getting of roots */
	roots = solver.Solve()
	if len(roots) != 2 {
		test.Errorf("Uncorrect amount of roots!")
	} else if math.Abs(roots[0]-1.28) > prec {
		test.Errorf("The first answer is uncorrect! Value is %f, but must be 1.28", roots[0])
	} else if math.Abs(roots[1]-7.51) > prec {
		test.Errorf("The second answer is uncorrect! Value is %f, but must be 7.51", roots[1])
	}

	/* changing of function to solve */
	function = func(x float64) float64 {
		return (x - 8.46) * (x - 0.56)
	}

	/* create new instance of structure to solve */
	solver, err = MakeSolver(function, begin, end, step, prec)
	/* checking for creation of structure */
	if err != nil {
		test.Errorf("Running of module was with mistake!")
	}

	/* checking for getting of roots */
	roots = solver.Solve()
	if len(roots) != 2 {
		test.Errorf("Uncorrect amount of roots!")
	} else if math.Abs(roots[0]-0.56) > prec {
		test.Errorf("The first answer is uncorrect! Value is %f, but must be 0.56", roots[0])
	} else if math.Abs(roots[1]-8.46) > prec {
		test.Errorf("The second answer is uncorrect! Value is %f, but must be 8.46", roots[1])
	}

}
