package integration

import (
	"fmt"
	"math"
)

/* initial number of divisions of interval for integration */
const divider = 10.0

/* structure to find integral for some interval */
type Integrator struct {
	function   func(x float64) float64
	begin, end float64
	prec       float64
}

/* function to make structure of integration */
func MakeIntegrator(function func(x float64) float64, begin, end, prec float64) (*Integrator, error) {

	/* verification of incoming data */
	if function == nil {
		return nil, fmt.Errorf("Function must not be nil!")
	} else if begin > end {
		return nil, fmt.Errorf("End must be larger than begin!")
	} else if prec >= end-begin || prec <= 0 {
		return nil, fmt.Errorf("Precision must be smaller than end-begin and larger than 0!")
	}

	/* creation structure of integration */
	var integ *Integrator = new(Integrator)
	/* initialize values of instance */
	integ.function = function
	integ.begin = begin
	integ.end = end
	integ.prec = prec

	/* return instance of structure */
	return integ, nil

}

/* function to calculate integral */
func calculateIntegral(integ *Integrator, step float64) float64 {

	/* variable declarations */
	var (
		x        float64 = integ.begin // current point
		integral float64 = 0.0         // value of integral
	)

	for x < integ.end {
		/* use Simpson's formula for integration */
		integral += (step / 6.0) * (integ.function(x) +
			4.0*integ.function(x+step/2.0) + integ.function(x+step))
		x += step
	}

	/* return integral */
	return integral

}

/* method to get integral */
func (integ *Integrator) Integrate() float64 {

	/* variable declarations */
	var (
		/* initial step for integration*/
		step float64 = (integ.end - integ.begin) / divider
		/* integral with usual step */
		integralUsual float64 = calculateIntegral(integ, step)
		/* integral with the lower half step */
		integralExact float64 = calculateIntegral(integ, step/2.0)
	)

	/* integrate, until the Runge condition is true */
	for math.Abs(integralExact-integralUsual)/15.0 > integ.prec {
		/* reduce step twice and integrate again */
		step /= 2.0
		integralUsual = integralExact
		integralExact = calculateIntegral(integ, step/2.0)
	}

	/* return result */
	return integralExact

}
