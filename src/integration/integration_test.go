package integration

import (
	"math"
	"testing"
)

func TestIntegration(test *testing.T) {

	/* declarations of variables */
	var (
		begin    float64                 = 0.0   /* begin of interval*/
		end      float64                 = 1.0   /* end of interval */
		prec     float64                 = 0.001 /* precision of integration */
		function func(x float64) float64         /* function to integrate */
		err      error                           /* variable to catch error */
		integ    *Integrator                     /* instance of structure to integrate */
		res      float64                         /* obtained result */
	)

	/* polynomial to test */
	function = func(x float64) float64 {
		return 4.0 * x * x * x
	}

	/* create instance of structure to integrate */
	integ, err = MakeIntegrator(function, begin, end, prec)
	/* checking for creation of structure */
	if err != nil {
		test.Errorf(err.Error())
	}

	/* checking for getting of integral */
	res = integ.Integrate()
	if math.Abs(res-1.0) > prec {
		test.Errorf("Precison is not enough! Value is %f, but answer must be 1.0", res)
	}

	/* new polynomial to test */
	function = func(x float64) float64 {
		return 3.0 * x * x
	}

	/* create new instance of structure to integrate */
	integ, err = MakeIntegrator(function, begin, end, prec)
	/* checking for creation of structure */
	if err != nil {
		test.Errorf(err.Error())
	}

	/* checking for getting of integral */
	res = integ.Integrate()
	if math.Abs(res-1.0) > prec {
		test.Errorf("Precison is not enough! Value is %f, but answer must be 1.0", res)
	}

}
